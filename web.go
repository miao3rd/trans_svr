package main

import (
	stdContext "context"
	"encoding/base64"
	"time"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/recover"
)

const GET_ERRCODE_SUCC int = 0    // 成功
const GET_ERRCODE_PARAM int = -1  // 参数错误
const GET_ERRCODE_DATA int = -2   // 数据错误
const GET_ERRCODE_DETECT int = -3 // 语言类型错误
const GET_ERRCODE_TRANS int = -4  // 翻译错误

var g_iris = iris.New()
var g_projectId string

func WebInit(host, key, cer, projectID string) {
	g_iris.Use(recover.New())

	crs := func(ctx iris.Context) {
		ctx.Header("Access-Control-Allow-Origin", "*")
		ctx.Header("Access-Control-Allow-Credentials", "true")

		if ctx.Method() == iris.MethodOptions {
			ctx.Header("Access-Control-Methods",
				"GET, POST, PUT, PATCH, DELETE")

			ctx.Header("Access-Control-Allow-Headers",
				"Access-Control-Allow-Origin,Content-Type")

			ctx.Header("Access-Control-Max-Age",
				"86400")

			ctx.StatusCode(iris.StatusNoContent)
			return
		}

		ctx.Next()
	}

	usrAPI := g_iris.Party("/test", crs).AllowMethods(iris.MethodOptions)
	{
		usrAPI.Get("/trans", onTrans)       // ?text=x&target=x
		usrAPI.Get("/gettrans", onGetTrans) // ?text=x&target=x

		usrAPI.Post("/posttrans", onPostTrans) // ?text=x&target=x
	}

	g_projectId = projectID
	Logger().Infof("trans_svr projectID=%s run", projectID)

	if len(key) > 0 && len(cer) > 0 {
		g_iris.Run(iris.TLS(host, cer, key))
	} else {
		g_iris.Listen(host)
	}
}

func WebFini() {
	timeout := 10 * time.Second
	ctx, cancel := stdContext.WithTimeout(stdContext.Background(), timeout)
	defer cancel()
	// close all hosts.
	g_iris.Shutdown(ctx)
}

func onTrans(ctx iris.Context) {
	text := ctx.URLParam("text")
	target := ctx.URLParam("target")

	language, err := translateDetect(g_projectId, text)
	if err != nil {
		ctx.JSON(iris.Map{"succ": GET_ERRCODE_DETECT, "err": err.Error()})
		Logger().Warnf("get text:%s target:%s detect err:%s", text, target, err.Error())
		return
	}

	trans, err := translateText(g_projectId, language, target, text)
	if err != nil {
		ctx.JSON(iris.Map{"succ": GET_ERRCODE_TRANS, "err": err.Error()})
		Logger().Warnf("get text:%s target:%s language:%s err:%s", text, target, language, err.Error())
		return
	}
	Logger().Infof("trans language[%s => %s] %s => %s", language, target, text, trans)

	ctx.JSON(iris.Map{"succ": GET_ERRCODE_SUCC, "trans": trans})
}

func onGetTrans(ctx iris.Context) {
	text := ctx.URLParam("text")
	target := ctx.URLParam("target")

	decode, err := base64.URLEncoding.DecodeString(text)
	if err != nil {
		ctx.JSON(iris.Map{"succ": GET_ERRCODE_DATA, "err": err.Error()})
		Logger().Warnf("get text:%s target:%s err:%s", text, target, err.Error())
		return
	}

	source := string(decode)
	language, err := translateDetect(g_projectId, source)
	if err != nil {
		ctx.JSON(iris.Map{"succ": GET_ERRCODE_DETECT, "err": err.Error()})
		Logger().Warnf("get source:%s target:%s detect err:%s", source, target, err.Error())
		return
	}

	trans, err := translateText(g_projectId, language, target, source)
	if err != nil {
		ctx.JSON(iris.Map{"succ": GET_ERRCODE_TRANS, "err": err.Error()})
		Logger().Warnf("get text:%s target:%s source:%s language:%s err:%s", text, target, source, language, err.Error())
		return
	}
	Logger().Infof("get target[%s => %s] %s => %s", target, language, source, trans)

	encode := base64.URLEncoding.EncodeToString([]byte(trans))
	ctx.JSON(iris.Map{"succ": GET_ERRCODE_SUCC, "trans": encode})
}

func onPostTrans(ctx iris.Context) {
	text := ctx.PostValue("text")
	target := ctx.PostValue("target")

	trans, err := transLanguage(text, target)
	if err != nil {
		ctx.JSON(iris.Map{"succ": GET_ERRCODE_PARAM, "err": err.Error()})
		Logger().Warnf("post text:%s target:%s err:%s", text, target, err.Error())
		return
	}

	ctx.JSON(iris.Map{"succ": GET_ERRCODE_SUCC, "trans": trans})
	Logger().Infof("post text:%s target:%s => trans:%s", text, target, trans)
}
