module trans_svr

go 1.16

require (
	cloud.google.com/go/translate v1.2.0
	github.com/ajg/form v1.5.1 // indirect
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/imkira/go-interpol v1.1.0 // indirect
	github.com/jonboulle/clockwork v0.3.0 // indirect
	github.com/kataras/iris/v12 v12.1.8
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.6 // indirect
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/smartystreets/goconvey v1.7.2 // indirect
	github.com/valyala/fasthttp v1.37.0 // indirect
	github.com/xeipuuv/gojsonschema v1.2.0 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
	github.com/yudai/gojsondiff v1.0.0 // indirect
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	github.com/zput/zxcTool v1.3.10
	golang.org/x/text v0.3.7
	google.golang.org/api v0.84.0
	google.golang.org/genproto v0.0.0-20220617124728-180714bec0ad
)
