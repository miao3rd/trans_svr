package main

import (
	"fmt"
	"path"
	"runtime"
	"time"

	nested "github.com/antonfisher/nested-logrus-formatter"
	rotate "github.com/lestrrat-go/file-rotatelogs"
	"github.com/sirupsen/logrus"
	"github.com/zput/zxcTool/ztLog/zt_formatter"
)

var g_logger = logrus.New()

func Logger() *logrus.Logger {
	return g_logger
}

func LogInit(filePath string) {
	writer, _ := rotate.New(
		filePath+".%m%d%H%M",
		rotate.WithLinkName(filePath),
		rotate.WithMaxAge(time.Duration(72)*time.Hour),
		rotate.WithRotationTime(time.Duration(12)*time.Hour),
	)
	g_logger.Out = writer

	g_logger.SetLevel(logrus.TraceLevel)
	g_logger.SetReportCaller(true)

	var formatter = &zt_formatter.ZtFormatter{
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			filename := path.Base(f.File)
			return fmt.Sprintf("%s()", f.Function), fmt.Sprintf("%s:%d", filename, f.Line)
		},
		Formatter: nested.Formatter{
			NoColors:      true,
			HideKeys:      false,
			ShowFullLevel: true,
			//FieldsOrder: []string{"component", "category"},
		},
	}
	g_logger.SetFormatter(formatter)
}
