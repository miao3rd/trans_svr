package main

import "fmt"

func main() {
	LogInit("./trans_svr.log")

	cfg, err := LoadConfig()
	if err != nil {
		Logger().Error(err.Error())
		return
	}
	Logger().Info("trans_svr init")

	WebInit(fmt.Sprintf(":%d", cfg.Port), cfg.Key, cfg.Cer, cfg.ProjectID)
}
