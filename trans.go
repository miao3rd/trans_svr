package main

import (
	"context"
	"fmt"

	trans "cloud.google.com/go/translate"
	translate "cloud.google.com/go/translate/apiv3"
	"golang.org/x/text/language"
	translatepb "google.golang.org/genproto/googleapis/cloud/translate/v3"
)

// detectLanguage detects the language of a text string.
func translateDetect(projectID string, text string) (string, error) {
	ctx := context.Background()
	client, err := translate.NewTranslationClient(ctx)
	if err != nil {
		return "", fmt.Errorf("NewTranslationClient: %v", err)
	}
	defer client.Close()

	req := &translatepb.DetectLanguageRequest{
		Parent:   fmt.Sprintf("projects/%s/locations/global", projectID),
		MimeType: "text/plain", // Mime types: "text/plain", "text/html"
		Source: &translatepb.DetectLanguageRequest_Content{
			Content: text,
		},
	}

	resp, err := client.DetectLanguage(ctx, req)
	if err != nil {
		return "", fmt.Errorf("DetectLanguage: %v", err)
	}

	languages := resp.GetLanguages()
	if len(languages) == 0 {
		return "", fmt.Errorf("%s detect language none", text)
	}

	return languages[0].GetLanguageCode(), nil
}

func translateText(projectID string, sourceLang string, targetLang string, text string) (string, error) {
	ctx := context.Background()
	client, err := translate.NewTranslationClient(ctx)
	if err != nil {
		return "", fmt.Errorf("NewTranslationClient: %v", err)
	}
	defer client.Close()

	req := &translatepb.TranslateTextRequest{
		Parent:             fmt.Sprintf("projects/%s/locations/global", projectID),
		SourceLanguageCode: sourceLang,
		TargetLanguageCode: targetLang,
		MimeType:           "text/plain", // Mime types: "text/plain", "text/html"
		Contents:           []string{text},
	}

	resp, err := client.TranslateText(ctx, req)
	if err != nil {
		return "", fmt.Errorf("TranslateText: %v", err)
	}

	trans := resp.GetTranslations()
	if len(trans) == 0 {
		return "", fmt.Errorf("%s trans language none", text)
	}

	// Display the translation for each input text provided
	return trans[0].GetTranslatedText(), nil
}

// export GOOGLE_APPLICATION_CREDENTIALS="/Users/dxl/work/goTest/asdxl198618-9f8873c64f0b.json"

func detectLanguage(text string) (*trans.Detection, error) {
	ctx := context.Background()
	client, err := trans.NewClient(ctx)
	if err != nil {
		return nil, err
	}
	defer client.Close()

	lang, err := client.DetectLanguage(ctx, []string{text})
	if err != nil {
		return nil, err
	}
	return &lang[0][0], nil
}

func transLanguage(text, target string) (string, error) {
	ctx := context.Background()

	lang, err := language.Parse(target)
	if err != nil {
		return "", fmt.Errorf("language.Parse: %v", err)
	}

	client, err := trans.NewClient(ctx)
	if err != nil {
		return "", err
	}
	defer client.Close()

	resp, err := client.Translate(ctx, []string{text}, lang, nil)
	if err != nil {
		return "", fmt.Errorf("Translate: %v", err)
	}
	if len(resp) == 0 {
		return "", fmt.Errorf("Translate returned empty response to text: %s", text)
	}
	return resp[0].Text, nil
}
