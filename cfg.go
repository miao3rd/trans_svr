package main

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	Port      int
	Key       string
	Cer       string
	ProjectID string
}

func LoadConfig() (*Config, error) {
	data, err := ioutil.ReadFile("./trans_svr.json")
	if err != nil {
		return nil, err
	}

	cfg := new(Config)
	err = json.Unmarshal(data, cfg)
	return cfg, err
}
